<?php

namespace eezeecommerce\SitemapBundle\Controller;

use eezeecommerce\ProductBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/sitemap.xml", name="sitemap_generator")
     */
    public function indexAction(Request $request)
    {
        $doc = new \DOMDocument("1.0");
        $doc->formatOutput = true;

        $urlset = $doc->createElementNS("http://www.sitemaps.org/schemas/sitemap/0.9", "urlset");
        $doc->appendChild($urlset);

        $router = $this->get("router");
        $collection = $router->getRouteCollection();

        $allRoutes = $collection->all();

        foreach($allRoutes as $route => $params) {

            if (preg_match("/(_assetic_)/", $route)
                || preg_match("/(_wdt)/", $route)
                || preg_match("/(_profiler)/", $route)
                || preg_match("/(_configurator)/", $route)
                || preg_match("/(_twig)/", $route)
                || preg_match("/(sitemap_generator)/", $route)) {
                continue;
            }

            $hostname = "https://".$request->getHost();

            $defaults = $params->compile();

            $page = $defaults->getStaticPrefix();
            if (preg_match("/(\/payment)/", $page)
                || preg_match("/(\/group)/", $page)
                || preg_match("/(\/admin)/", $page)
                || preg_match("/(\/update)/", $page)
                || preg_match("/(\/add)/", $page)
                || preg_match("/(\/delete)/", $page)
                || preg_match("/(\/account\/)/", $page)
                || preg_match("/(\/cart\/remove)/", $page)
                || preg_match("/(\/cart\/add\/)/", $page)
                || preg_match("/(\/checkout)/", $page)
                || preg_match("/(\/ordersuccess)/", $page)
                || preg_match("/(\/register\/)/", $page)
                || preg_match("/(\/resetting\/)/", $page)
                || preg_match("/(\/profile)/", $page)
                || preg_match("/(\/login_check)/", $page)
                || preg_match("/(\/logout)/", $page)) {
                continue;
            }

            $root = $doc->createElement("url");
            $urlset->appendChild($root);

            $loc = $doc->createElement("loc");
            $loc = $root->appendChild($loc);

            $url = $doc->createTextNode($hostname.$page);
            $url = $loc->appendChild($url);

            $date = $doc->createElement("lastmod");
            $date = $root->appendChild($date);

            $dateclass = date("Y-m-d");

            $lastMod = $doc->createTextNode($dateclass);
            $lastMod = $date->appendChild($lastMod);

            $change = $doc->createElement("changefreq");
            $change = $root->appendChild($change);

            $freq = $doc->createTextNode("monthly");
            $freq = $change->appendChild($freq);

            $pri = $doc->createElement("priority");
            $pri = $root->appendChild($pri);

            $priority = $doc->createTextNode("0.8");
            $priority = $pri->appendChild($priority);

            if (preg_match("/(\/products)/", $page)) {
                $products = $this->getDoctrine()->getRepository("eezeecommerceProductBundle:Product")
                    ->findAll();

                foreach($products as $product) {
                    $root = $doc->createElement("url");
                    $urlset->appendChild($root);

                    $loc = $doc->createElement("loc");
                    $loc = $root->appendChild($loc);

                    $url = $doc->createTextNode($hostname."/products/".$product->getUri()->getSlug());
                    $url = $loc->appendChild($url);

                    $date = $doc->createElement("lastmod");
                    $date = $root->appendChild($date);

                    $dateclass = date("Y-m-d");

                    $lastMod = $doc->createTextNode($dateclass);
                    $lastMod = $date->appendChild($lastMod);

                    $change = $doc->createElement("changefreq");
                    $change = $root->appendChild($change);

                    $freq = $doc->createTextNode("monthly");
                    $freq = $change->appendChild($freq);

                    $pri = $doc->createElement("priority");
                    $pri = $root->appendChild($pri);

                    $priority = $doc->createTextNode("0.8");
                    $priority = $pri->appendChild($priority);
                }
            }

        }

        $response = new Response();
        $response->setContent($doc->saveXml());
        $response->headers->set("Content-Type", "text/xml");

        return $response;
    }

    /**
     * @Route("/products.xml", name="product_feed_generator")
     */
    public function productFeedAction()
    {
        $date = date("Y-m-d");

        $doc = new \DOMDocument("1.0", "utf-8");
        $doc->formatOutput = true;

        $rss = $doc->createElement("rss");
        $rss->setAttribute("version", "2.0");
        $rss->setAttribute("xmlns:g", "http://base.google.com/ns/1.0");
        $doc->appendChild($rss);

        $root = $doc->createElement("channel");
        $rss->appendChild($root);


        $item = $doc->createElement("title");
        $root->appendChild($item);

        $text = $doc->createTextNode("Allsportsawards Product DataFeed");
        $item->appendChild($text);

        $item = $doc->createElement("link");
        $root->appendChild($item);
        $item->setAttribute("rel", "self");
        $item->setAttribute("href", "https://www.allsportsawards.co.uk");
        /*
                $item = $doc->createElement("updated");
                $root->appendChild($item);

                $text = $doc->createTextNode($date);
                $item->appendChild($text);

                $author = $doc->createElement("author");
                $root->appendChild($author);

                $name = $doc->createElement("name");
                $author->appendChild($name);

                $text = $doc->createTextNode("Allsportsawards");
                $name->appendChild($text);

                $item = $doc->createElement("id");
                $root->appendChild($item);

                $text = $doc->createTextNode("tag:allsportsawards.com,".$date.":/products");
                $item->appendChild($text);
                */

        $products = $this->getDoctrine()->getRepository("eezeecommerceProductBundle:Product")
            ->findAll();

        foreach($products as $product) {
            if($product instanceof Product) {

            }

            $images = $product->getImage();

            if (count($images) < 1) {
                continue;
            }

            $image = $images[0]->getImageName();

            // Start Feed
            $entry = $doc->createElement("item");
            $root->appendChild($entry);

            $item = $doc->createElement("title");
            $entry->appendChild($item);

            $text = $doc->createTextNode($product->getShortDescription());
            $item->appendChild($text);

            $item = $doc->createElement("g:condition");
            $entry->appendChild($item);

            $text = $doc->createTextNode("new");
            $item->appendChild($text);

            $item = $doc->createElement("g:availability");
            $entry->appendChild($item);

            $text = $doc->createTextNode("in stock");
            $item->appendChild($text);

            $item = $doc->createElement("link");
            $entry->appendChild($item);
            $item->setAttribute("rel", "self");
            $item->setAttribute("href", "https://www.allsportsawards.co.uk/products/".$product->getUri()->getSlug());

            $item = $doc->createElement("description");
            $entry->appendChild($item);

            $text = $doc->createTextNode($product->getLongDescription());
            $item->appendChild($text);

            $item = $doc->createElement("id");
            $entry->appendChild($item);

            $text = $doc->createTextNode($product->getId());
            $item->appendChild($text);

            $item = $doc->createElement("g:image_link");
            $entry->appendChild($item);

            $text = $doc->createTextNode("https://www.allsportsawards.co.uk/uploads/".$image);
            $item->appendChild($text);

            $item = $doc->createElement("mpn");
            $entry->appendChild($item);

            $text = $doc->createTextNode($product->getStockCode());
            $item->appendChild($text);

            $item = $doc->createElement("g:price");
            $entry->appendChild($item);

            $text = $doc->createTextNode($product->getBasePrice()." GBP");
            $item->appendChild($text);

            $item = $doc->createElement("g:product_type");
            $entry->appendChild($item);

            $text = $doc->createTextNode("Trophies");
            $item->appendChild($text);

            $item = $doc->createElement("g:google_product_category");
            $entry->appendChild($item);

            $text = $doc->createTextNode("988");
            $item->appendChild($text);

            $item = $doc->createElement("g:identifier_exists");
            $entry->appendChild($item);

            $text = $doc->createTextNode("FALSE");
            $item->appendChild($text);

            $shipping = $doc->createElement("g:shipping");
            $entry->appendChild($shipping);

            $item = $doc->createElement("g:country");
            $shipping->appendChild($item);

            $text = $doc->createTextNode("GB");
            $item->appendChild($text);

            $item = $doc->createElement("g:service");
            $shipping->appendChild($item);

            $text = $doc->createTextNode("DPD");
            $item->appendChild($text);

            $item = $doc->createElement("g:price");
            $shipping->appendChild($item);

            $text = $doc->createTextNode("6.75 GBP");
            $item->appendChild($text);
        }


        $response = new Response();
        $response->setContent($doc->saveXml());
        $response->headers->set("Content-Type", "application/xml; charset=utf-8");

        return $response;
    }
}
